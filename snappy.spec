Name:		snappy
Version:	1.1.9
Release:	2
Summary:	A fast compressor/decompressor
License:	BSD
URL:		https://github.com/google/snappy
Source0:	https://github.com/google/snappy/archive/%{version}/%{name}-%{version}.tar.gz
Source1:	snappy.pc

Patch0:		remove-dependency-on-google-benchmark-and-gmock.patch
Patch1:		fix-the-AdvanceToNextTag-fails-to-be-compiled-without-inline.patch
Patch2:		add-option-to-enable-rtti-set-default-to-current-ben.patch	

BuildRequires:	gcc-c++ make gtest-devel cmake

%description
Snappy is a compression/decompression library. It does not aim for maximum compression,
or compatibility with any other compression library; instead, it aims for very high
speeds and reasonable compression.

%package        devel
Summary:        Development files for snappy
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       cmake-filesystem pkgconfig

%description    devel
This package is the development files for snappy.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake  -DSNAPPY_ENABLE_RTTI=ON 
%make_build

%install
rm -rf %{buildroot}
mkdir %{buildroot}
%make_install
install -p -D %{SOURCE1} %{buildroot}%{_libdir}/pkgconfig/snappy.pc

%check
make test

%pre

%preun

%post	-p /sbin/ldconfig

%postun	-p /sbin/ldconfig

%files
%defattr(-,root,root)
%license COPYING AUTHORS
%{_libdir}/libsnappy.so.*

%files devel
%defattr(-,root,root)
%doc format_*.txt framing_*.txt
%{_includedir}/snappy*.h
%{_libdir}/libsnappy.so
%{_libdir}/pkgconfig/snappy.pc
%{_libdir}/cmake/Snappy/*.cmake

%files help
%doc NEWS README.md

%changelog
* Wed Jun 22 2022 wangzengliang<wangzengliang1@huawei.com> - 1.1.9-2
- DESC: add option to enable rtti set default to current

* Fri Dec 24 2021 yuanxin<yuanxin24@huawei.com> - 1.1.9-1
- update version to 1.1.9

* Thu Dec 2 2021 hanxinke<hanxinke@huawei.com> - 1.1.8-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add testcases and delete unnecessary test code for snappy

* Fri Jul 17 2020 shixuantong <shixuantong> - 1.1.8-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update to 1.1.8-1

* Wed Oct 9 2019 shenyangyang<shenyangyang4@huawei.com> - 1.1.7-10
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:modify the patch number and requires of devel

* Fri Sep 27 2019 shenyangyang<shenyangyang4@huawei.com> - 1.1.7-9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:move the license

* Tue Sep 24 2019 shenyangyang<shenyangyang4@huawei.com> - 1.1.7-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.7-7
- Package init
